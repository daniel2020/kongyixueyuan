package main

import "fmt"

func main() {

	var age int 
	age = 100
	fmt.Println(age)

	age = 200
	fmt.Println(age)

	var age2 int = 30
	fmt.Println(age2)

	var num = 11
	fmt.Println(num)

	num2 := 333
	fmt.Println(num2)

	var a, b, c int
	a = 3
	b = 4
	c = 5
	fmt.Println(a, b, c, "aaaa")

	var d, e, f int = 6, 7, 8
	fmt.Println(d, e, f)


	var str, n = 100, "apple"
	fmt.Println(str, n)
	name, age3 := "张三", 28
	fmt.Println(name, age3)


	var(
		name2 = "李四"
		age4 = 40
	)
	fmt.Println(name2, age4)

}