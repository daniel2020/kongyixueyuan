
package main

import "fmt"

func main() {
	// var a = 1;
	// var b = "hellow";
	// var c = true;
	// var d = 1.2;

	// fmt.Println(a);
	// fmt.Println(b);
	// fmt.Println(c);
	// fmt.Println(d);

	// fmt.Println(&d)

	// const a1 = "abc";
	// const b1 string = "def";

	// fmt.Println(a1)
	// fmt.Println(b1)


	// const LENGTH int = 10
	// const WIDTH int = 5
	// var area int
	// const a, b, c = 1, "hello", true

	// area = LENGTH * WIDTH;

	// fmt.Println(LENGTH)
	// fmt.Println(WIDTH)
	// fmt.Println(area)

	// fmt.Println(a, b, c)


/*	const (
		a = 11
		b
		c = "hello2"
		d
	)

	fmt.Printf("b:%T,\t %v\n", b, b)
	fmt.Printf("d:%T,\t %v\n", d, d)
*/

/*	const (
		a = iota
		b //= iota
		// c //= iota
	)
	fmt.Println(a)
	fmt.Println(b)
	// fmt.Println(c)
*/


	const (
		a = 99
		b = iota
		c 
		d = iota
		e = "hello"
		f
		g = iota
	)

	fmt.Println("a:", a)
	fmt.Println("b:", b)
	fmt.Println("c:", c)
	fmt.Println("d:", d)
	fmt.Println("e:", e)
	fmt.Println("f:", f)
	fmt.Println("g:", g)


}
