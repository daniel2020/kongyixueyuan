package main

import "fmt"

func main() {
	//创建map
	var map1  map[int]string
	map2 := map[int]string{1:"王者农药", 2:"绝地求生", 3:"连连看", 4:"创奇霸业", }
	map3 := make(map[int]string)

	fmt.Println(map1)
	fmt.Println(map2)
	fmt.Println(map3)

	fmt.Println("map1,len:", len(map1))
	fmt.Println("map2,len:", len(map2))
	fmt.Println("map3,len:", len(map3))

	//2 nil map
	fmt.Println(map1 == nil, map2==nil, map3==nil)		//true false false
	fmt.Printf("%p,%p,%p\n", map1, map2, map3)

	fmt.Println("======================")
	//3.存储键值对
	if (map1 == nil) {
		fmt.Println("map1上未创建....")
		map1 = make(map[int]string)
	}
	map1[1] = "王二狗"
	map1[2] = "daniel"
	map1[3] = "Lucy"
	fmt.Println(map1)

	//4.获取map中的数据
	fmt.Println(map1[2])
	fmt.Println(map1[20])

	val, ok := map1[20]
	if ok == true {
		fmt.Println(val)
	} else {
		fmt.Println("获取的key不存在,没有对应value")
	}

	//4.删除某个键值对
	delete(map1, 3)
	fmt.Println(map1)
	//5.根据key修改value值
	map1[3] = "如花"
	fmt.Println(map1)


}

