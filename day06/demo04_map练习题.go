package main

import "fmt"

/*
  1.创建一个map用于存储一个人的信息：
"name"：王二狗
"age"：30
sex：男性
address："北京市xx路xx号"

2.打印遍历该map
3.修改sex为女性
4.如果想map中添加重复的key，会如何？
*/
func main () {
	//创建map
	user := map[string]string{}
	//存数据
	user["name"] = "王二狗"
	user["age"] = "30"
	user["sex"] = "男性"
	user["address"] = "北京市海淀区五道口"
	//取数据  遍历
	for key, val := range user {
		fmt.Println(key, val)
	}

	//修改
	user["sex"] = "女性"
	fmt.Println(user)

	//加入重复的key
	//user["age"] = "29"
	fmt.Println(user)
	fmt.Println(user)
	fmt.Println(user)
	fmt.Println(user)
	fmt.Println(user)

}










