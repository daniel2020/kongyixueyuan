package main

import (
	"fmt"
	"sort"
	"time"
)

func main () {
/*	map2 := map[int]string {1:"王者农药",2:"绝地求生",3:"连连看",4:"传奇霸业",5:"消消乐"}
	fmt.Println("map的长度：", len(map2))

	//把map的key取出放入slice中
	s1 := make([]int, 0, len(map2))
	for key := range map2 {
		s1 = append(s1, key)
	}

	fmt.Println(s1)
	//给s1排序
	sort.Ints(s1)
	fmt.Println(s1)

	//key的顺序遍历map
	for _,k := range s1 {
		fmt.Println(k, map2[k])
	}

	str := []string{"Go", "Bravo", "Gopher", "Alpha", "Grin", "Delta"}
	sort.Strings(str)
	fmt.Println(str)*/

	//test1()
	test2()

}

//遍历map
func test1 () {
	map1 := map[int]string { 4:"guangzhaou", 1:"beijing",  5:"hanzhou", 2:"shenzhen", 3:"shanghai"}
	fmt.Println(map1)

	//取出key
	keys := make([]int, 0, len(map1))
	for key := range map1 {
		keys = append(keys, key)
	}

	//对key排序
	sort.Ints(keys)

	//按keys顺序取map value
	for _,key := range keys {
		fmt.Println(key, map1[key])
	}

	fmt.Println(time.Now())
}

//排序
func test2 () {
	ints := []int{2, 5, 3, 1, 9, 7}
	fmt.Println(ints)
	sort.Ints(ints)
	fmt.Println(ints)

	strs := []string {"shanghai", "beijing", "shenzhen", "guangzhou"}
	fmt.Println(strs)
	sort.Strings(strs)
	fmt.Println(strs)

	floats := []float64{3.14, 1.2, 2.5, 9.11, 1.10}
	fmt.Print(floats)
	sort.Float64s(floats)
	fmt.Print(floats)


}
















