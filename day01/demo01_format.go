package main

import "fmt"

func main() {
	var i = 30
	
	fmt.Printf("整数30---10进制:%d\n", i)
	fmt.Printf("整数30---2进制 :%b\n", i)
	fmt.Printf("整数30---8进制 :%o\n", i)
	fmt.Printf("整数30---16进制:%x\n", i)


	var flag1 = true
	fmt.Println("变量flag的值:", flag1, 22)		//Println函数会把参数自动拼接??
	fmt.Printf("变量flag的值:%t\n", flag1)
	fmt.Printf("数值是:%t, 类型是:%T\n", flag1, flag1)

	fmt.Printf("%T\n", 11)   // %T  取类型
}