package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//pratise()
	//encrypt()
	//pratise06()
	//s1 := pratise06_sl(10)
	//pratise07(s1)
	//fmt.Println(s1)

	//pratise08()

	pratise09()
}

func pratise() {
	//1.创建int类型数组，并存储5个数值，打印输出
	arrInt := [5] int{1,2,3,4,5}
	fmt.Println(arrInt)
	fmt.Println("\n===============")

	//2.创建string类型数组，并存储3个字符串，打印输出
	arrStr := [3] string{"this ", "is", "long time"}
	for i:=0; i<len(arrStr); i++ {
		fmt.Print(arrStr[i], " ")
	}
	fmt.Println("\n===================")

	//3.创建float64类型数组，并存储4个浮点小数，打印输出
	arrFloat := [4] float32{3.14, 2.15, 7.99, 0.555}
	for i:=0; i<len(arrFloat); i++ {
		fmt.Print(arrFloat[i], " ")
	}
	fmt.Println("\n================")

	//4.使用range打印以上数组
	fmt.Println("========用range遍历数组===========")
	for index, value := range arrInt {
		fmt.Println(index, value)
	}
	fmt.Println("=============")

	for _, value := range arrStr {
		fmt.Println(value)
	}
	fmt.Println("================")

	for _, value := range arrFloat {
		fmt.Println("v:", value)
	}
	fmt.Println("=====================")

}


func encrypt() {
	//5.题目：某个公司采用公用电话传递数据，
	// 数据是四位的整数，
	//在传递过程中是加密的，加密规则如下：
	//
	// 每位数字都加上5,
	//然后用和，除以10的余数代替该数字，   (x + 5) / 10 =y ...a
	// 再将第一位和第四位交换，
	// 第二位和第三位交换。

	numSource := 7294
	fmt.Println("加密前：", numSource)
	//拆分
	a := numSource / 1000
	b := numSource / 100 % 10
	c := numSource / 10 % 10
	d := numSource % 10
	fmt.Println("拆分 :", a, b, c, d)

	//
	a = (a + 5) % 10
	b = (b + 5) % 10
	c = (c + 5) % 10
	d = (d + 5) % 10
	fmt.Println("", a, b, c, d)

	//交换位置
	a,d = d, a
	b,c = c, b
	fmt.Println("交换 :", a, b, c, d )

	//拼接
	encryptNum := a*1000 + b*100 + c*10 + d
	fmt.Println(encryptNum)

}

func pratise06() {
	//6.给定一个整型数组，长度为10。数字取自随机数。

	rand.Seed(time.Now().UnixNano())

	nums := [10] int{}

	for i:=0; i<len(nums); i++ {
		nums[i] = rand.Intn(100)
	}

	fmt.Println("随机数数组：",nums)
}

//---------用切片存放随机数
func pratise06_sl(n int)  []int {
	//6.给定一个整型数组，长度为10。数字取自随机数。

	rand.Seed(time.Now().UnixNano())

	nums := []int{}

	for i:=0; i<10; i++ {
		nums = append(nums, rand.Intn(100))
	}
	fmt.Println("随机数切片：",nums)
	return nums
}

func pratise07(arr []int) []int  {
	//7.将作业6的数组进行从大到小排序。
	for i:=1; i<len(arr); i++ {
		for j:=0; j<len(arr)-i; j++ {
			if arr[j] < arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
	return arr
}

func pratise08() {
	//8.创建一个切片，并向切片中存储10个数字，打印输出。
	length := 10
	slice := []int{}
	for i:=0; i<length; i++ {
		slice = append(slice, i)
	}

	for i:=0; i<len(slice); i++ {
		fmt.Print(slice[i], " ")
	}

}

/**
扩展题：
有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），
凡报到3的人退出圈子，问最后留下1个人的是原来第几号的那位。
 */
func pratise09() {
	//初始化
	const manNum = 7
	arr := [manNum]bool{}
	for i:=0; i<len(arr); i++ {
		arr[i] = true
	}

	count := 0	//数数的计数器 1-3
	index := 0	//遍历数组
	remain := len(arr) //统计余下人数
	for ;remain > 1; {

		//人在
		if arr[index] {
			count++

			if count == 3 {		//数到3
				arr[index] = false
				count = 0
				remain--
			}
		}

		index++		//1,2,3
		if index == len(arr) {
			index = 0
		}
		//fmt.Println(arr)
	}

	//输出结果
	for i:=0; i<len(arr); i++ {
		if arr[i] {
			fmt.Println(i+1)
		}
	}
	fmt.Println(arr)



}












