package main

import "fmt"

func main() {
	/*
	排序：从小到大(升序)，或者从大到小(降序)
		让数组中的数据按照从小到大排列。
	 */
/*	arr1 := [5]int{15, 23, 8, 10, 7}
	fmt.Println(arr1)

	for j:= 1; j < len(arr1); j++ {				//外层循环 控制比较的第几轮
		for i:=0; i<5-j; i++ {
			if arr1[i] > arr1[i+1] {
				arr1[i], arr1[i+1] = arr1[i+1], arr1[i]
			}
			fmt.Println("--" ,j,i,"arr:",  arr1)
		}
		fmt.Println(j,"arr:",  arr1)
	}*/

	sortBubble()
}

func sortBubble () {
	arr1 := [6]int{99, 15, 23, 8, 10, 7}
	fmt.Println(arr1)

	for i:=1; i<len(arr1); i++ {
		for j:=0; j<len(arr1)-i; j++ {
			if arr1[j] > arr1[j+1] {
				arr1[j], arr1[j+1] = arr1[j+1], arr1[j]
			}
		}
 	}
 	fmt.Println(arr1)
}


