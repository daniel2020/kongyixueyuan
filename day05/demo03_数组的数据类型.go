package main

import "fmt"

func main() {
	a := [5]int{1,2,3,4,5}
	b := [4]string{"this", "is", "golang", "code"}

	fmt.Println(a)
	fmt.Println(b)
	fmt.Printf("a数组的类型：%T\n", a)
	fmt.Printf("b数组的类型：%T\n", b)

	c := [2]int{1,3}
	fmt.Printf("c数组的类型：%T\n", c)

	d := [2]int{5,6}
	c = d
	//c = a

	num1 := 10
	num2 := 20
	//num3 := "haha"

	num1 = num2
	num2 = 100
	fmt.Println(num1, num2)

	fmt.Println("===============")
	arr1 := [...]string{"Rose", "jack", "王晓波"}
	fmt.Println("arr1:", arr1)
	arr2 := arr1
	fmt.Println("arr2:", arr2)
	arr1[0] = "hhhhhh"
	fmt.Println(arr1)
	fmt.Println(arr2)

	fmt.Printf("arr1地址%P\n", arr1)
	fmt.Printf("arr2地址%p\n", arr2)

}















