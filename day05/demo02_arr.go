package main

import (
	"fmt"
	"time"
)

func main() {	//遍历数组
	var arr = [...] int{6,2,4,9,8,3}

	//遍历方式1
	for i:=0; i<len(arr); i++{
		fmt.Print(arr[i], " ")
	}
	fmt.Println()

	//遍历方式2   range遍历叔祖
	for index, value := range arr {
		fmt.Println(index, value)
	}
	fmt.Println(time.Now())

	for _, value := range arr {
		fmt.Println(value)
	}
	fmt.Println(time.Now().UnixNano())
}

