package main

import "fmt"

/*
练习1：创建一个int类型的数组，存储4个数。分别进行赋值，并打印结果。
练习2：创建一个double类型的数组，存储2个小数。
练习3：创建一个String类型的数组，存储3个字符串。
练习4：给定一个数组，arr1 := [10] int{5,4,3,7,10,2,9,8,6,1}，求数组中所有数据的总和。
 */
func main () {
	var arr1 = [4]int{10, 20, 30, 40}

	for i:=0; i<len(arr1); i++ {
		fmt.Println(arr1[i])
	}
	fmt.Println("=============")

	var arr2 = []float32{1.2, 3.14}
	fmt.Println(arr2[0])
	fmt.Println(arr2[1])
	fmt.Println("================")

	var arr3  = [...]string{"hello", "golang", "test..."}
	for i:=0; i<len(arr3); i++ {
		fmt.Println(arr3[i])
	}
	fmt.Println("============")


	arr4 := [10] int{5,4,3,7,10,2,9,8,6,1}
	var sum = 0;

	for i:=0; i<len(arr4); i++ {
		sum += arr4[i]
	}
	fmt.Println("数组arr4的总和:", sum)

}
