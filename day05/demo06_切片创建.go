package main

import "fmt"

func main()  {
	//数组
	a := [4]int{1, 2, 3, 4}
	fmt.Println(len(a))
	fmt.Println("数组的：", len(a), cap(a))


	//切片
	s1 := []int{1,2,3,4,5,6,7,8}
	fmt.Println(s1)
	fmt.Println("切片：", len(s1), cap(s1))
	fmt.Printf("%T\n", s1)

	s2 := []int {} //创建一个空的切片
	fmt.Println(len(s2), cap(s2))

	fmt.Println("=============")

	//2 使用make([]T, len, cap)
	s3 := make([]int, 3, 8)
	fmt.Println(s3)
	fmt.Println(len(s3), cap(s3))
	fmt.Printf("%T\n", s3)
	fmt.Println("--------------------------")

	//在数组的基础上创建切片
	b := [10]int{1,2,3,4,5,6,7,8,9,10}
	fmt.Println(b)
	s4 := b[2:6];	//数组名 [start: end] //3,4,5,6,
	fmt.Println(s4)
	fmt.Println(len(s4), cap(s4))
	s5 := b[:6]		//从头开始切
	s6 := b[6:]		//切到最后
	fmt.Println(s5)
	fmt.Println(s6)

	//s7 := b[4:12]  //invalid slice index 12 (out of bounds for 10-element array)


}





