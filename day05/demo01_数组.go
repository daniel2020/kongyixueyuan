package main

import "fmt"

func main() {
	/*

	var nums [10] int
	var nums2 = []int{9, 8, 7, 6, 5}
	var nums3 = []float32{1.9 , 2, 3} //-----------

	//取值
	fmt.Println(nums)
	fmt.Println(nums2)
	fmt.Println(nums3)

	fmt.Println(nums3[1])
	//fmt.Println(nums3[10])

	fmt.Println("数组长度:", len(nums3))
	//遍历

	for i:=0; i< len(nums3); i++ {
		fmt.Println(nums3[i])
	}

	//数组初始化
	var a[] int
	fmt.Println(a)

	var b = [5]int{1,2,3,4,5}
	fmt.Println(b)

	//var c = []int{11, 22, 33}
	var c = [...]int{11, 22, 33}
	fmt.Println(c)

	fmt.Println("cap:", cap(c))

	fmt.Println("============")

	//数组其他语法格式
	var aa [4] int
	fmt.Println("aa:", aa)

	//数组创建时同时初始化
	var bb = [5]int{1, 2, 3, 4, 5}
	fmt.Println("bb:", bb)

	var cc = [5]int{1, 2, 3}
	fmt.Println("cc:", cc)

	var dd = [5]int{4:100}
	fmt.Println("dd:", dd)

	var ee = [5]int{1:3, 3:8}
	fmt.Println("ee:", ee)

	//省略var
	ff := [5] string{"rose", "lisi", "王五", "王二狗", "二蛋"}
	fmt.Println("ff:", ff)

	gg := [...] int{1, 2, 3, 5, 7}
	fmt.Println("gg:", gg)
	fmt.Println(len(gg))

	hh := [...] int{1:100, 4:400, 9:900}
	fmt.Println("hh:", hh)
	fmt.Println(len(hh))
	*/
	
	
	test00()	
	//test01()
	//test02()
	//test03()

}

func test00() {
	fmt.Println("========1111...222====")
}

func test01() {
	fmt.Println("========1111====")
	var a [] int
	b := [4]int{2, 5}
	c := [4]int{5, 3: 10}
	d := [...]int{1, 2, 3}
	e := [...]int{10, 3:100}

	fmt.Println(a, b, c, d, e)

}

func test02 () {
	type user struct {
		name string
		age int
	}

	d := [...]user{
		{"Tom", 20},
		{"Mary", 18},
	}

	fmt.Println(d)

}

func test03 () {
	var a, b [2]int
	println(a == b)
	fmt.Println(a, "--", b)

	c := [2]int{1, 2}
	d := [2]int{1, 3}
	fmt.Println(c == d)

	var e, f [2]map[string]int
	//fmt.Println(e == f)

}













