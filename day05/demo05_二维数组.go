package main

import (
	"fmt"
)

func main() {
	a := 10
	fmt.Println(a)

	//一维
	b := []int{1, 2, 3}
	fmt.Println(b)

	//二维
	c := [][]int{
		{1,2,3},
		{4,5,6},
		{7,8,9},
		{10,11,13}}
	fmt.Println(c)
	fmt.Println( "二维数组长度:",len(c))
	fmt.Println( "数组c[0]长度:",len(c[0]))
	fmt.Println(c[2])

	fmt.Println("=========")
	//遍历二维数组
	for i:=0; i<len(c); i++ {
		for j:=0; j<len(c[i]); j++ {
			fmt.Print(c[i][j], " ")
		}
	}
	fmt.Println("")


}








